
<?php
    // router.php
    //           ?cantidad=15

    // Ejemplos de rewrite: (.htaccess)
    // 
    // localhost/.../listas/   => localhost/.../listas/router.php?cantidad=
    // localhost/.../listas/15 => localhost/.../listas/router.php?cantidad=15

    define('BASE_URL', '//'.$_SERVER['SERVER_NAME'] . ':' . $_SERVER['SERVER_PORT'] . dirname($_SERVER['PHP_SELF']).'/');



    function elementoListaHTML($elemento){
        return "<li>" . $elemento . "</li>";
    }

    function listaHTML($arreglo, $limite){
        $elementos_html = "";
        
        $cant_elementos = count($arreglo);

        for($i = 0; $i < $cant_elementos && $i < $limite; $i++){
            $elemento = $arreglo[$i];
            $elementos_html = $elementos_html . elementoListaHTML($elemento);
        }
        
        return "<ol>" . $elementos_html . "</ol>";
    }

    $arreglo = [1,2,3,4,5,6,8,9,0,1,2,3,4,5,6,8,9,0,1,2,3,4,5,6,8,9,0,1,2,3,4,5,6,8,9,0,1,2,3,4,5,6,8,9,0];
    
    $limite = count($arreglo);

    if(isset($_GET) && count($_GET)>0){
        if(isset($_GET["cantidad"]) && $_GET["cantidad"] != ""){
            $cantidad = $_GET["cantidad"];
            echo "<p>Cantidad a mostrar: " . $cantidad . "<br>";
            $limite = $cantidad;
        }
    }
    echo listaHTML($arreglo, $limite);
?>
<html>
    <body>
        <a href="<?=BASE_URL?>5">Mostrar  5 elementos</a><br> 
        <a href="<?=BASE_URL?>10">Mostrar 10 elementos</a><br> 
        <a href="<?=BASE_URL?>15">Mostrar 15 elementos</a><br>
        <a href="<?=BASE_URL?>">Mostrar todos</a><br> 
    </body>
</html>